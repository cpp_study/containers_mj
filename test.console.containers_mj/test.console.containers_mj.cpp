// test.console.containers_mj.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include"../containers_mj/list_mj.h"
#include <vector>

int main()
{
	std::cout << "std::vector creation (100)";
	auto start = std::chrono::high_resolution_clock::now();
	auto v = std::vector<int>(100);
	auto finish = std::chrono::high_resolution_clock::now();
	auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);

	std::cout << "std::vector creation finish: "<< nanoseconds.count() <<"ns\n" ;
	std::cout << "\n";
	std::cout << "list_mj creation (100)";

	start = std::chrono::high_resolution_clock::now();
	auto list = containers_mj::list_mj<int>(100);
	finish = std::chrono::high_resolution_clock::now();
	nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);

	std::cout << "list_mj creation finish: " << nanoseconds.count() << "ns\n";
	
	std::cout << "\n";

	for (size_t i = 0; i < 100; i++)
	{
		std::cout << "vector push\n";
		start = std::chrono::high_resolution_clock::now();
		v.push_back(1);
		finish = std::chrono::high_resolution_clock::now();
		nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);

		std::cout << "vector push finish: " << nanoseconds.count() << "ns\n";
	}
	

	std::cout << "list_mj push";
	for (size_t i = 0; i < 100; i++)
	{
		start = std::chrono::high_resolution_clock::now();
		list.push(i);
		finish = std::chrono::high_resolution_clock::now();
		nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);

		std::cout << "list_mj push finish: " << nanoseconds.count() << "ns\n";
	}
	
	std::cout << "\n";
	std::cout << "vector pop";
	
	start = std::chrono::high_resolution_clock::now();
	v.pop_back();
	finish = std::chrono::high_resolution_clock::now();
	nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);

	std::cout << "vector pop finish: " << nanoseconds.count() << "ns\n";
	std::cout << "\n";
	std::cout << "list_mj pop";

	start = std::chrono::high_resolution_clock::now();
	list.pop();
	finish = std::chrono::high_resolution_clock::now();
	nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);

	std::cout << "list_mj pop finish: " << nanoseconds.count() << "ns\n";
	std::cout << "\n";
	std::cout << "vector remove at N/A\n";
	std::cout << "\n";
	std::cout << "list_mj remove at\n";
	start = std::chrono::high_resolution_clock::now();
	list.remove_at(45);
	finish = std::chrono::high_resolution_clock::now();
	nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);

	std::cout << "list_mj remove at finish: " << nanoseconds.count() << "ns\n";

	system("pause");

    return 0;
}

