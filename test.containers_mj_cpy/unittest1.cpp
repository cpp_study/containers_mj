#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\containers_mj\list_mj.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace testcontainers_mj
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		//not actual ut just a method I use to test on the go in early phase
		TEST_METHOD(dupa)
		{
			auto target = containers_mj::list_mj<int>(4);
			target.push(4);
			auto y = target[0];
			Assert::AreEqual(4, target[0]);
		}

		TEST_METHOD(push_first)
		{
			auto target = containers_mj::list_mj<int>(4);
			target.push(4);
			Assert::AreEqual(4, target[0]);
		}

		TEST_METHOD(count)
		{
			auto target = containers_mj::list_mj<int>(4);

			target.push(4);
			Assert::AreEqual(size_t(1), target.get_count());
		}

		TEST_METHOD(get_at_0)
		{
			auto target = containers_mj::list_mj<int>(4);
			target.push(4);
			Assert::AreEqual(4, target[0]);
		}

		TEST_METHOD(get_fail)
		{
			auto target = containers_mj::list_mj<int>(4);
			target.push(4);
			auto temp = target[1];
			Assert::AreEqual(4, target[0]);
		}

		TEST_METHOD(extend)
		{
			auto target = containers_mj::list_mj<int>(1);
			target.push(4);
			target.push(5);

			Assert::AreEqual(size_t(3), target.get_size());
		}

		TEST_METHOD(extend_2)
		{
			auto target = containers_mj::list_mj<int>(2);
			target.push(4);
			target.push(5);
			target.push(6);

			Assert::AreEqual(size_t(5), target.get_size());
		}

		TEST_METHOD(push_3)
		{
			auto target = containers_mj::list_mj<int>(3);
			target.push(4);
			target.push(5);
			target.push(6);
			
			Assert::AreEqual(4, target[0]);
			Assert::AreEqual(5, target[1]);
			Assert::AreEqual(6, target[2]);
			Assert::AreEqual(size_t(3), target.get_size());
		}

		TEST_METHOD(push_extend)
		{
			auto target = containers_mj::list_mj<int>(2);
			target.push(4);
			target.push(5);
			target.push(6);

			Assert::AreEqual(4, target[0]);
			Assert::AreEqual(5, target[1]);
			Assert::AreEqual(6, target[2]);
			Assert::AreEqual(size_t(5), target.get_size());
		}

		TEST_METHOD(pop)
		{
			auto target = containers_mj::list_mj<int>(3);
			target.push(4);
			target.push(5);
			target.push(6);
			auto res = target.pop();

			Assert::AreEqual(4, target[0]);
			Assert::AreEqual(5, target[1]);
			Assert::AreEqual(size_t(2), target.get_count());
			Assert::AreEqual(size_t(0), res);
		}

		TEST_METHOD(pop_fail)
		{
			auto target = containers_mj::list_mj<int>(3);
			auto res = target.pop();


			Assert::AreEqual(size_t(0), target.get_count());
			Assert::AreEqual(size_t(-1), res);
		}

		TEST_METHOD(remove_at)
		{
			auto target = containers_mj::list_mj<int>(3);
			target.push(4);
			target.push(5);
			target.push(6);
			auto res = target.remove_at(1);

			Assert::AreEqual(4, target[0]);
			Assert::AreEqual(6, target[1]);
			Assert::AreEqual(size_t(2), target.get_count());
			Assert::AreEqual(size_t(0), res);
		}

		TEST_METHOD(remove_at_fail)
		{
			auto target = containers_mj::list_mj<int>(3);
			target.push(4);
			target.push(5);
			target.push(6);
			auto res = target.remove_at(3);

			Assert::AreEqual(4, target[0]);
			Assert::AreEqual(5, target[1]);
			Assert::AreEqual(6, target[2]);

			Assert::AreEqual(size_t(3), target.get_count());
			Assert::AreEqual(size_t(-1), res);
		}

		TEST_METHOD(copy_construction)
		{
			auto target = containers_mj::list_mj<int>(2);
			target.push(4);
			target.push(5);
			auto cpy = containers_mj::list_mj<int>(target);

			Assert::AreEqual(4, cpy[0]);
			Assert::AreEqual(4, target[0]);

			Assert::AreEqual(5, cpy[1]);
			Assert::AreEqual(5, target[1]);

			Assert::AreEqual(size_t(2), cpy.get_size());			
			Assert::AreEqual(size_t(2), target.get_size());

			Assert::IsFalse(&target == &cpy);
		}
	};
}