#pragma once 
#include "stdafx.h"

namespace containers_mj {

	//20180107_mj: exercise to implement lightweight growing array
	template<class  T>
	class list_mj {
	public:

		explicit list_mj(size_t size) {
			_size = size;
			_data = new T[_size];
			_current = &_data[-1];
		};

		list_mj(const list_mj& org) {
			_size = org._size;
			_data = new T[_size];

			for (size_t i = 0; i < _size; i++)
			{
				_data[i] = org._data[i];
			}

			_current = &_data[get_count()];
		}

		size_t get_count() const {
			return _current + 1 - _data;
		}

		size_t get_size() {
			return _size;
		}

		void push(const T& next) {
			_current++;

			if (get_count() > _size) {
				extend();
			}

			*_current = next;
		}

		size_t pop() {
			if (get_count() > 0) {
				_current--;

				return 0;
			}

			return -1;
		}

		size_t remove_at(size_t index) {
			if (index < get_count() && index >= 0) {
				for (size_t i = index; i < get_count(); i++)
				{
					_data[i] = _data[i + 1];
				}

				_current--;

				return 0;
			}			

			return -1;
		}

		T& operator[](size_t index){
			return _data[index];
		}

		~list_mj() {
			delete[] _data;
		}

	private:
		size_t _size;
		T* _current;
		T* _data;

		void extend() {
			T* new_data = new T[_size * 2 + 1];

			for (size_t i = 0; i < _size; i++)
			{
				new_data[i] = _data[i];
			}

			T* clean_up = _data;
			_data = new_data;
			_current = &_data[_size];
			_size = _size * 2 + 1;
			
			delete[] clean_up;
		}
	};
	
}